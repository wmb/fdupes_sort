/*
 * fdupes_sort: sort the output of `fdupes -S' by size
 * Copyright (C) 2018  Walid M. Boudelal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fdupes_sort.h"

#define _POSIX_C_SOURCE 200809L

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool check_size(const char *line);
unsigned get_size(const char *line);
char *get_human_size(const unsigned s);

struct lines *lines_add_line(struct lines *lines, const char *line,
	const size_t *len);
struct dupe *dupes_add_dupe(struct dupe *dupes,
	unsigned s, unsigned n, struct lines *l);

struct dupe *dupes_add_dupe_sorted(struct dupe *dupes,
	unsigned s, unsigned n, struct lines *l);

struct dupe *get_dupes(FILE *f)
{
	char *line = NULL;
	size_t nn = 0;
	struct dupe *dupes = NULL;
	unsigned line_number = 0;

	bool next_is_size = true;

	unsigned size;
	unsigned ndups;

	unsigned *s = NULL;
	unsigned *n = NULL;

	struct lines *lines = NULL;

	while (++line_number, getline(&line, &nn, f) != -1) {
		if (next_is_size) {
			if (check_size(line) == false) {
				fprintf(stderr, "get_dupes: invalid "
					"size on line %u\n",
					line_number);
				goto cont;
			}
			size = get_size(line);

			s = &size;
			n = &ndups;
			*n = 0;

			next_is_size = false;

			goto cont;
		}

		/* if we got here, we must already have the size */
		if (!s) {
			fputs("get_dupes: size undefined\n", stderr);
			free(line);
			free_lines(lines);
			return dupes;
		}

		if (*line != '\n') {
			/* line is not empty -- add it */
			lines = lines_add_line(lines, line, &nn);
			++*n;
		} else {
			/* line is empty */
			dupes = dupes_add_dupe_sorted(dupes, *s, *n, lines);

			s = NULL;
			n = NULL;
			lines = NULL;

			next_is_size = true;
		}

cont:
		if (line) {
			free(line);
			line = NULL;
		}
		nn = 0;
	}

	if (s && n && lines) {
			dupes = dupes_add_dupe_sorted(dupes, *s, *n, lines);
			s = NULL;
			n = NULL;
			lines = NULL;
	}

	return dupes;
}

/* Check that line is actualy the line containing the size,
 * i.e., that it matches the regexp: /^(\d+) bytes? each:$/
 * But we will not be using regexes for simplicity's sake
 * and also because I don't know how to use them in C,
 * and I can't be arsed to learn.
 */
bool check_size(const char *line)
{
	bool valid = true;
	const char *l;

	for (l = line; *l && isdigit(*l); ++l)
		;

	if (line == l) {
		/* No digits read, therefore invalid */
		valid = false;
	}

	if (strcmp(l, " bytes each:\n") == 0)
		valid = valid && true;
	else if (strcmp(line, "1 byte each:\n") == 0)
		valid = true;
	else
		valid = false;


	return valid;
}

/* Get the size from the line containing the size.
 * This assumes that the line in question does actually
 * contain the size (this can be verified with the check_size()
 * function above).
 */
unsigned get_size(const char *line)
{
	unsigned size;
	size_t size_len;
	char *size_str;

	for (size_len = 0; isdigit(line[size_len]); ++size_len)
		;

	size_str = malloc(1 + size_len);
	if (!size_str)
		abort();

	strncpy(size_str, line, size_len);
	size_str[size_len] = '\0';

	size = (unsigned) strtoul(size_str, NULL, 10);

	free(size_str);

	return size;
}

struct lines *lines_add_line(struct lines *lines, const char *line,
	const size_t *len)
{
	struct lines *p;
	struct lines *l = malloc(sizeof(struct lines));
	char *myline;

	if (!l) {
		fputs(__func__, stderr);
		perror(": can't allocate memory for l");
		return lines;
	}

	myline = malloc(*len);
	if (!myline) {
		free(l);
		fputs(__func__, stderr);
		perror(": can't allocate memory for myline");
		return lines;
	}

	strncpy(myline, line, *len);

	l->line = myline;
	l->next = NULL;

	if (lines == NULL)
		return l;

	for (p = lines; p->next; p = p->next)
		;

	p->next = l;

	return lines;
}

struct dupe *dupes_add_dupe(struct dupe *dupes,
	unsigned s, unsigned n, struct lines *l)
{
	struct dupe *p;
	struct dupe *d = malloc(sizeof(struct dupe));

	if (!d)
		abort();

	d->size = s;
	d->ndups = n;
	d->lines = l;
	d->next = NULL;

	if (dupes == NULL)
		return d;

	for (p = dupes; p->next; p = p->next)
		;

	p->next = d;

	return dupes;
}
struct dupe *dupes_add_dupe_sorted(struct dupe *dupes,
	unsigned s, unsigned n, struct lines *l)
{
	struct dupe *d = malloc(sizeof(struct dupe));
	struct dupe *p;

	if (!d)
		abort();

	d->size = s;
	d->ndups = n;
	d->lines = l;

	if (dupes == NULL) {
		d->next = NULL;
		return d;
	}

	if (dupes->size < d->size) {
		/* d is the first becuase it's the biggest */
		d->next = dupes;
		return d;
	}

	for (p = dupes; p->next && p->next->size > d->size; p = p->next)
		;

	d->next = p->next;
	p->next = d;

	return dupes;
}

void print_dupes(const struct dupe *dupes)
{
	while (dupes) {
		const unsigned s = dupes->size;
		const unsigned n = dupes->ndups;
		const struct lines *l = dupes->lines;
		char *const human_s = get_human_size(s);

		printf("%s\t(%u dupes):\n", human_s, n);
		free(human_s);
		while (l) {
			fputs(l->line, stdout);
			l = l->next;
		}
		putchar('\n');

		dupes = dupes->next;
	}
}

char *get_human_size(const unsigned s)
{
	char *hs;
	size_t len;
	unsigned char selected = 0;
	double amount;
	const char *units[] = {
		"KiB",
		"MiB",
		"GiB"
	};

	const double kb = 1 << 10;
	const double mb = 1 << 20;
	const double gb = 1 << 30;

	if (s >= .9 * kb)
		++selected;
	if (s >= .9 * mb)
		++selected;
	if (s >= .9 * gb)
		++selected;

	if (selected) {
		amount = s / (selected > 2 ? gb
				: selected > 1 ? mb
				: kb);
		len = snprintf(NULL, 0, "%.2f %s",
			amount, units[selected - 1]);
	} else {
		len = snprintf(NULL, 0, "%u byte%s", s, s == 1 ? "" : "s");
	}

	hs = malloc(++len);
	if (!hs)
		return NULL;

	if (selected)
		snprintf(hs, len, "%.2f %s", amount, units[selected - 1]);
	else
		snprintf(hs, len, "%u byte%s", s, s == 1 ? "" : "s");

	hs[len - 1] = '\0';

	return hs;
}

void free_dupes(struct dupe *dupes)
{
	if (dupes->next)
		free_dupes(dupes->next);

	if (dupes->lines)
		free_lines(dupes->lines);

	free(dupes);
}

void free_lines(struct lines *lines)
{
	if (lines->next)
		free_lines(lines->next);

	if (lines->line)
		free(lines->line);

	free(lines);
}
