/*
 * fdupes_sort: sort the output of `fdupes -S' by size
 * Copyright (C) 2018  Walid M. Boudelal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FDUPES_SORT_H
#define FDUPES_SORT_H

#include <stdio.h>

struct lines {
	char *line;
	struct lines *next;
};

struct dupe {
	unsigned size;
	unsigned ndups;
	struct lines *lines;
	struct dupe *next;
};

struct dupe *get_dupes(FILE *f);

void free_dupes(struct dupe *dupes);

void free_lines(struct lines *lines);

void print_dupes(const struct dupe *dupes);

void sort_dupes_by_size(struct dupe *dupes);

#endif
