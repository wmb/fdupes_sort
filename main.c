/*
 * fdupes_sort: sort the output of `fdupes -S' by size
 * Copyright (C) 2018  Walid M. Boudelal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fdupes_sort.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(const char *progname);

int main(int argc, char *argv[])
{
	FILE *in;
	struct dupe *dupes;

	if (argc == 1
		|| (argc == 2 && strcmp(argv[1], "-") == 0)) {
		/* read from stdin */
		in = stdin;
	} else if (argc == 2
		&& strcmp(argv[1], "-h")
		&& strcmp(argv[1], "--help")) {
		/* read from the named file */
		in = fopen(argv[1], "r");

		if (!in) {
			fputs(argv[0], stderr);
			fputs(": ", stderr);
			perror(argv[1]);
			exit(EXIT_FAILURE);
		}
	} else {
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	dupes = get_dupes(in);

	print_dupes(dupes);

	free_dupes(dupes);

	if (in != stdin)
		fclose(in);

	exit(EXIT_SUCCESS);
}

void usage(const char *p)
{
	const char *const usage =
"USAGE:\n"
	"\tfdupes -rS | %s -\n"
	"\t%s <INFILE >OUTFILE\n"
	"\t%s - <INFILE >OUTFILE\n"
	"\t%s INFILE >OUTFILE\n"
	"\t%s -h\n"
	"\t%s --help\n"
"\n"
"Where INFILE is the output of the fdupes(1) program"
" when run with the -S (--size) flag.\n"
"\"-\" designates standard input.\n"
"\n";
	printf(usage, p, p, p, p, p, p);

	return;
}
